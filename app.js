import express from "express";
import sequelize from './connection/postgresconn'
import mongoDB from './connection/mongoconn'
import job from './jobs/duedates.job'
import api from './api'
import cors from 'cors'


const router = express.Router();
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

const port = process.env.PORT || 4000;



app.use('/', api());



app.listen(port, () => {
  console.log(`server running on port ${port}`);
  
});


export default ({res,data={},schema,next}) => {
    const option = {
      abortEarly: false,
      allowUnknown: true,
      //stripUnknown: true,
    };
    const { error, value } = schema.validate(data, option);
    if (error) {
      const errorMessage = error.details.map((x) => x.message).join(", ");
      res.status(400).send(errorMessage);
    } else {
      next();
    }
}
export default {
    DATABASE_QUERY: 'Error executing database query',
    AUTHENTICATION: 'Email/password not valid',
    UNDEFINE: 'Error unknow',
    EMAIL_DUPLICATED: 'Email already in use',
}
export default   ({
  message,
  errorstatus = 500,
  stracktrace = "",
}) => {
  const error = {
    message: message || ErrorTypes.UNDEFINE,
    errorstatus,
    stracktrace,
  };

  
  return error;
};

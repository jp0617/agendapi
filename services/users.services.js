import UserModel from "../models/user.model";
import CustomError from "../utils/CustomError";
import ErrorTypes from "../utils/ErrorTypes";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import environment from "../config/environment";
import logServices from "./log.services";

const validate = async ({ email, password }) => {
  try {
    //TODO: generar token
    
    const user = await UserModel.findOne({ email });
    
    if (user) {
      const match = await bcrypt.compare(password, user.password);
      console.log(match)
      if (match) {
        const payload = {
          idUser: user.id,
          role: user.role,
        };
        await logServices.create({who:user.id,log:"LOGIN USER"})
        const token = jwt.sign(payload, environment.secret, {
          expiresIn: "12h",
        });
        
        return { token };
      }
    }

    throw CustomError({ message: "user no found", errorstatus: 401 });
  } catch (error) {
    throw CustomError({ message: error.message, errorstatus: 500 });
  }
};

const create = async ({ name, role, email, password }) => {
  try {
    const userCheckEmail = await UserModel.findOne({ email: email });
    if (userCheckEmail) {
      throw error;
    }
    const saltRouds = 10;
    const passwordencrypt = await bcrypt.hash(password, saltRouds);
    const user = UserModel({
      name,
      role,
      email,
      password: passwordencrypt,
    });
    await user.save();

    return user;
  } catch (error) {
    throw error;
  }
};

const getAll = async () => {
  try {
    const user = await UserModel.find();
    return user;
  } catch (error) {
    throw error;
  }
};

const profile = async ({ id = "60a46aac7125233f44bb7646" }) => {
  try {
    const user = await UserModel.findOne({ _id: id });
    return user;
  } catch (error) {
    throw error;
  }
};

export default {
  create,
  validate,
  getAll,
  profile,
};

import { LogAgenda } from "../models/log.model";
import CustomError from "../utils/CustomError";
import ErrorTypes from "../utils/ErrorTypes";
const create = async({who="",log=""}) => {
    try {
        const when=(new Date()).toISOString();
        const query = {
            when,
            who,
            log
        }
        const logRegister = await LogAgenda.create(query)
        console.log("logRegister.id", logRegister.id);
        return true;
    } catch (error) {
        console.log("error->",error)
        throw CustomError({
          message: error.message || ErrorTypes.DATABASE_QUERY,
          errorstatus: error.errorstatus,
          stacktrace: error.stacktrace || error,
        });
    }
};

export default { create };

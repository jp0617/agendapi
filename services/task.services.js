import taskModel from "../models/task.model";
import Error from "../utils/CustomError";
import ErrorTypes from "../utils/ErrorTypes";

const create = async ({
  title,
  due_date,
  description,
  responsible,
  collaborators,
}) => {
  try {
    const task = taskModel({
      title,
      due_date,
      description,
      responsible,
      collaborators,
    });
    await task.save();
    return task;
  } catch (error) {
    throw error;
  }
};
const getAll = async ({ status, due_date_init, due_date_end }) => {
  try {
    const query = {};

    if (status) query["status"] = status;
    if (due_date_end && due_date_init) {
      query["due_date_end"] = { $gte: due_date_init, $lte: due_date_end };
    }
    
    const tasks = await taskModel.find(query)
      .populate("responsible", "name email")
      .populate("collaborators", "name email")
      .exec();
    console.log(tasks)
    return tasks;
  } catch (error) {
    throw error;
  }
};
const detail = async (id) => {
  try {
    
    const task = await taskModel
      .findById(id)
      .populate("responsible", "name email")
      .populate("collaborators", "name email")
      .exec();
    return task;
  } catch (error) {
    throw error;
  }
};
const updatestatus = async ({ id, status }) => {
  try {
    const query = { _id: id };
    const update = { status };
    
      const task = await taskModel.findById(query);
      task.status = Number(status);
      await task.save();
    return {"update":"ok"};
  } catch (error) {
    throw error;
  }
};
export default {
  create,
  detail,
  getAll,
  updatestatus,
};

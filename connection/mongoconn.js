import mongose from "mongoose";
import environment from '../config/environment'
const url = environment.mongo_conn;
mongose.connect(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

const db = mongose.connection;

try {
    
    db.on("error", () => {
      console.log("error connecting with database");
    });
    db.once("open", () => {
      console.log("Success connecting with database");
    });
} catch (error) {
    throw error;
}

export default db;

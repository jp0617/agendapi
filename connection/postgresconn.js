import { Sequelize } from "sequelize";

import env from "../config/environment";


const sequelize = new Sequelize(env.postgres_conn);

const testConnection = async (sequelize) => {
  try {
    await sequelize.authenticate();
    console.log("connect postgres on");
  } catch (error) {
    console.log("error", error);
  }
};

testConnection(sequelize);

export default sequelize;

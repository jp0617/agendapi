import Router  from 'express'
import { authVerify } from '../../middlewares/auth.verify';
import usersControllers from '../../controllers/users.controllers';
const route = Router();

export default (app) => {
    app.use('/users', route)

    route.get('/profile',[authVerify],usersControllers.profile)

    route.post('/',usersControllers.create)

    route.get('/',[authVerify],usersControllers.getusers)
    
}
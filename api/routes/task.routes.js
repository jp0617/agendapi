import Router  from 'express'
import taskControllers from '../../controllers/task.controllers';
import { authVerify } from "../../middlewares/auth.verify";
import { TaskSchema } from '../middlewares/task.schema';
const route = Router();

export default (app) => {
    app.use('/tasks', route)

    route.get('/',[authVerify],taskControllers.getAll)

    route.get("/:id", [authVerify], taskControllers.detail);

    route.post("/create", [authVerify,TaskSchema], taskControllers.create);

    route.put("/status/:id", [authVerify], taskControllers.updatestatus);
}
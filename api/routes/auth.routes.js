import express from "express";
import authControllers from "../../controllers/auth.controllers";
import { authVerify } from "../../middlewares/auth.verify";
import { signinSchema } from "../middlewares/signin.shema";
const route = express.Router();

export default (app) => {
  app.use("/auth", route);

  route.post("/signup", authControllers.signup);
route.post("/check", [authVerify], authControllers.check);
  route.post("/signin", [signinSchema],authControllers.signin);
};

import  express  from 'express'
import authRoute from './routes/auth.routes.js';
import taskRoute from './routes/task.routes.js';
import userRoute from './routes/user.routes.js';
export default ()=>{
    const app = express.Router();

    authRoute(app)
    userRoute(app)
    taskRoute(app)
    

    return app
}
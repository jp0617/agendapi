import Joi from "joi";
import ValidationSchema from "../../utils/Validation.schema";
export const TaskSchema = (req, res, next) => {
  const schema = Joi.object({
    nombre: Joi.string().required(),
    description: Joi.string().required(),
    due_date: Joi.date().required(),
    collaborators: Joi.string().alphanum().required(),
    responsible: Joi.string().alphanum().required(),
  });
  ValidationSchema({ res, data: req.body, schema, next });
};

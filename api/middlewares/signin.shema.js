import Joi from "joi";
import ValidationSchema from "../../utils/Validation.schema";
export const signinSchema = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(6).required(),
  });
  ValidationSchema({res, data: req.body, schema, next})
};

import mongoose from "mongoose";
const { Schema } = mongoose;


  
  const taskSchema = new Schema(
    {
      title: String,
      description: String,
      due_date: Date,
      status: { type: Number, default: 1 },
      responsible: { type: Schema.Types.ObjectId, rer: "users" },
      collaborators: [{ type: Schema.Types.ObjectId, rer: "users" }],
    },
    {
      timestamps: true,
    }
  );


const task = mongoose.model("Task", taskSchema);
export default task;

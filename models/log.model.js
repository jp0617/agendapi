import { DataTypes } from "sequelize";
import sequelize from "../connection/postgresconn";

export const LogAgenda = sequelize.define("agendapp.logs", {
  who: { type: DataTypes.STRING },
  when: { type: DataTypes.STRING },
  log: { type: DataTypes.TEXT },
  id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
});

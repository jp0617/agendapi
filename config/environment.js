import dotenv from "dotenv";
import path from 'path'
dotenv.config(/*{ path: path.resolve(path.dirname(''), './.env') }*/);


export default {
  secret: process.env.JWT_SECRET,
  mongo_conn: process.env.MONGO_CONN,
  postgres_conn: process.env.POSTGRES_CONN,
};

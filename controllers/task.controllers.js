import taskservices from "../services/task.services";

const getAll = async (req, res) => {
  try {
    const query = req.query;
    const tasks = await taskservices.getAll(query);
    res.status(200).json(tasks);
  } catch (error) {
    res.send(error);
  }
};

const detail = async (req, res) => {
  try {
    const { id } = req.params;
    const task = await taskservices.detail(id);
    res.json(task);
  } catch (error) {
    res.send(error);
  }
};

const create = async (req, res) => {
  try {
    const taskData = req.body;
    const task = await taskservices.create(taskData);
    res.json(task);
  } catch (error) {
    res.send(error);
  }
};

const updatestatus = async (req, res) => {
  try {
    let { status } = req.query;
    const { id } = req.params;
    status = Number(status);
    const taskdata = { status, id };
    const task = await taskservices.updatestatus(taskdata);
    res.json(task);
  } catch (error) {
    res.send(error);
  }
};

export default {
  updatestatus,
  create,
  detail,
  getAll,
};

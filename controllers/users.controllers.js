import usersService from "../services/users.services";

const getusers = async(req, res) => {
    try {
      const user = await usersService.getAll();
      res.json(user);
    } catch (error) {
      res.send(error);
    }
}

const profile = async(req, res) => {
  try {
    const { idUser } = req.user;
      res.send(idUser)
      // const user = await usersService.profile();
      // res.json(user);
    } catch (error) {
     console.log(error)
      res.send(error);
    }
}


const create = (req, res) => {
    res.send('create')
}

export default {
    getusers,
    profile,
    create
}
import usersService from "../services/users.services";

const signin = async (req, res) => {
  try {
    const credentials = req.body;
    
    const response = await usersService.validate(credentials);
    res.status(200).json(response);
  } catch (error) {
    res.status(error.errorstatus).send(error.message);
  }
};

const signup = async (req, res) => {
  try {
    const userData = req.body;
    const user = await usersService.create(userData);
    res.json(user);
  } catch (error) {
    res.status(error.errorstatus).send(error.message);
  }
};

const check = async (req, res) => {
  res.json({ 'auth': true })
}

export default {
  signin,
  signup,
  check
};
